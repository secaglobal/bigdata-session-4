#!/bin/bash

sed s/127.0.0.1/$NODE_IP/ /etc/riak/riak.conf.template > /etc/riak/riak.conf

service riak start

if [ -n $SEED_NODE_IP ]; then
    riak-admin cluster join riak@$SEED_NODE_IP
    riak-admin cluster plan
    riak-admin cluster commit
fi

while true; do sleep 1000; done


