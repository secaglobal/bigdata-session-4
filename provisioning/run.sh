#!/usr/bin/env bash
docker network create --driver=bridge --subnet=172.16.238.0/24 --gateway=172.16.238.1 hadoop

docker run  -d \
            --name riak-1 \
            -h 'riak-1' \
            -e NODE_IP=172.16.238.20 \
            --ip=172.16.238.20 \
            --net=hadoop \
            riak

docker run  -d \
            --name riak-2 \
            -h 'riak-2' \
            -e NODE_IP=172.16.238.21 \
            -e SEED_NODE_IP=172.16.238.20 \
            --ip=172.16.238.21 \
            --net=hadoop \
            riak


docker run  -d \
            --name riak-3 \
            -h 'riak-3' \
            -e NODE_IP=172.16.238.22 \
            -e SEED_NODE_IP=172.16.238.20 \
            --ip=172.16.238.22 \
            --net=hadoop \
            riak


